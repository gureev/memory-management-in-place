#include "ph/ph.hpp"

#include "sources/memory_manager.hpp"

//------------------------------------------------------------------------------

MemoryManager::MemoryManager( MemoryPtr _memoryPtr, int _memorySize, int _blockSize )
	:	m_memoryPtr{ _memoryPtr }
	,	m_freeBlock{ _memoryPtr }
	,	m_memorySize{ _memorySize }
	,	m_blockSize{ _blockSize }
{
	assert( m_memorySize > 0 );
	assert( m_blockSize > 0 );
	assert( m_memorySize >= getActualBlockSize() );
	assert( !( m_memorySize % getActualBlockSize() ) );
}

//------------------------------------------------------------------------------

MemoryManager::~MemoryManager() = default;

//------------------------------------------------------------------------------

void *
MemoryManager::allocate()
{
	auto nextFreeBlock{ fetchNextFreeBlock() };
	if ( !nextFreeBlock )
		return nullptr;

	m_freeBlock = nextFreeBlock;
	* nextFreeBlock = BusyBlockMark;

	return convertToUserFriendlyPointer( nextFreeBlock );
}

//------------------------------------------------------------------------------

void
MemoryManager::free( void * _block )
{
	if ( !_block )
		return;

	auto concreteBlock{ convertFromUserFriendlyPointer( _block ) };

	assert( hasBlock( concreteBlock ) );
	assert( isBlockBusy( concreteBlock ) );

	* concreteBlock = FreeBlockMark;
}

//------------------------------------------------------------------------------

int
MemoryManager::getBlocksCount() const
{
	return m_memorySize / getActualBlockSize();
}

//------------------------------------------------------------------------------

int
MemoryManager::getFreeBlocksCount() const
{
	// NOTE: Now used only for tests, no need to cache result

	int freeBlocksCount{ 0 };

	const int blocksCount{ getBlocksCount() };
	for ( int i{ 0 }; i < blocksCount; ++i )
		if ( isBlockFree( getBlock( i ) ) )
			++freeBlocksCount;

	return freeBlocksCount;
}

//------------------------------------------------------------------------------

int
MemoryManager::getBusyBlocksCount() const
{
	return getBlocksCount() - getFreeBlocksCount();
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockBusy( void * _block ) const
{
	return isBlockBusy( convertFromUserFriendlyPointer( _block ) );
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockFree( void * _block ) const
{
	return isBlockFree( convertFromUserFriendlyPointer( _block ) );
}

//------------------------------------------------------------------------------

MemoryManager::MemoryPtr
MemoryManager::getBlock( int _index ) const
{
	assert( _index >= 0 );
	assert( _index < getBlocksCount() );
	return m_memoryPtr + _index * getActualBlockSize();
}

//------------------------------------------------------------------------------

int
MemoryManager::getActualBlockSize() const
{
	return m_blockSize + 1;
}

//------------------------------------------------------------------------------

bool
MemoryManager::hasBlock( MemoryPtr _block ) const
{
	return	_block >= m_memoryPtr
		&&	_block < ( m_memoryPtr + m_memorySize )
	;
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockBusy( MemoryPtr _block ) const
{
	assert( hasBlock( _block ) );
	return * _block == BusyBlockMark;
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockFree( MemoryPtr _block ) const
{
	assert( hasBlock( _block ) );
	return !isBlockBusy( _block );
}

//------------------------------------------------------------------------------

MemoryManager::MemoryPtr
MemoryManager::fetchNextFreeBlock() const
{
	auto nextFreeBlock{ m_freeBlock };

	const int blocksCount{ getBlocksCount() };
	for ( int i{ 0 }; i < blocksCount; ++i )
	{
		if ( * nextFreeBlock == FreeBlockMark )
			return nextFreeBlock;

		nextFreeBlock += getActualBlockSize();
		if ( nextFreeBlock >= m_memoryPtr + m_memorySize )
			nextFreeBlock = m_memoryPtr;
	}

	return nullptr;
}

//------------------------------------------------------------------------------

void *
MemoryManager::convertToUserFriendlyPointer( MemoryPtr _block ) const
{
	return static_cast< void * >( _block + 1 );
}

//------------------------------------------------------------------------------

MemoryManager::MemoryPtr
MemoryManager::convertFromUserFriendlyPointer( void * _block ) const
{
	return static_cast< MemoryPtr >( _block ) - 1;
}

//------------------------------------------------------------------------------
