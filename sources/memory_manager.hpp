#ifndef __MEMORY_MANAGER_HPP__
#define __MEMORY_MANAGER_HPP__

//------------------------------------------------------------------------------

class MemoryManager
{

//------------------------------------------------------------------------------
	
public:

//------------------------------------------------------------------------------

	using MemoryType = std::byte;
	using MemoryPtr = MemoryType *;

//------------------------------------------------------------------------------

	MemoryManager( MemoryPtr _memoryPtr, int _memorySize, int _blockSize );

	~MemoryManager();

//------------------------------------------------------------------------------

	[[ nodiscard ]]
	void * allocate();

	void free( void * _block );

//------------------------------------------------------------------------------

	int getBlocksCount() const;

	int getFreeBlocksCount() const;

	int getBusyBlocksCount() const;

//------------------------------------------------------------------------------

	bool isBlockBusy( void * _block ) const;

	bool isBlockFree( void * _block ) const;

//------------------------------------------------------------------------------

private:

//------------------------------------------------------------------------------

	MemoryPtr getBlock( int _index ) const;

	int getActualBlockSize() const;

//------------------------------------------------------------------------------

	bool hasBlock( MemoryPtr _block ) const;

	bool isBlockBusy( MemoryPtr _block ) const;

	bool isBlockFree( MemoryPtr _block ) const;

//------------------------------------------------------------------------------

	MemoryPtr fetchNextFreeBlock() const;

//------------------------------------------------------------------------------

	void * convertToUserFriendlyPointer( MemoryPtr _block ) const;

	MemoryPtr convertFromUserFriendlyPointer( void * _block ) const;

//------------------------------------------------------------------------------

private:

//------------------------------------------------------------------------------

	MemoryPtr m_memoryPtr;

	MemoryPtr m_freeBlock;

	int m_memorySize;

	int m_blockSize;

//------------------------------------------------------------------------------

	static constexpr MemoryType BusyBlockMark{ 1 };

	static constexpr MemoryType FreeBlockMark{ 0 };

//------------------------------------------------------------------------------
	
};

//------------------------------------------------------------------------------

#endif // __MEMORY_MANAGER_HPP__
